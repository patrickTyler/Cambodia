import numpy
import pandas
import os
import statsmodels.api as sm
import statsmodels.formula.api as smf
import sys
from sklearn.model_selection import ShuffleSplit
from sklearn.preprocessing import MinMaxScaler
from matplotlib import pyplot
from keras.models import Sequential
from keras.layers import Dense, Flatten
from keras.layers import LSTM

from scipy.stats import linregress
from sklearn import linear_model
from seq2seq import SimpleSeq2Seq, Seq2Seq, AttentionSeq2Seq
from sklearn.preprocessing import normalize

class MonthData:
    def __init__(self, x, y):
        self.x = x
        self.y = y

def importDengueIndyVariables(csvFile):
    dataframe=pandas.read_csv(csvFile, usecols=[3,4,5,6,7,8,9,10,11,12,13,14,15,16,17],
                                index_col=False,engine='python')
    scaler = MinMaxScaler(feature_range=(0, 1))
    dataframe = numpy.array(scaler.fit_transform(dataframe))
    return dataframe

def importDengueIncidenceRates(csvFile):
    dataframe = pandas.read_csv(csvFile, usecols=[3],
                                index_col=False, engine='python')
    dataframe = numpy.array(dataframe)
    return dataframe

def calculateCorrelation(indyVariable, dependentVariable, indyVarName):
    slope, intercept, r_value, p_value, std_err = linregress(indyVariable, dependentVariable)
    #print(str(indyVarName) + ": " + str(p_value))
    return p_value

#Calculate the expectation of y given x
#   That is- E(Y | x) = e ^ Theta.transposex
def expectationOfYGivenX(regressionModel, x):
    x =1
#60 x 12 x 4
#Converts array into timestep of 12 months
def convertToSeries(x, y):
    monthTensorX = None
    monthTensorY = None
    dengueTensorX = None
    dengueTensorY = None
    monthCounter = 0
    print("x.shape: "  + str(x.shape))
    print("y shape: " + str(y.shape))
    for i in range(x.shape[0]):
        #Create month
        monthCounter +=1
        reducedX = numpy.zeros((1,1,4))
        reducedX[0,0,0] = x[i,5]
        reducedX[0,0,1] = x[i,7]
        reducedX[0,0,2] = x[i,13]
        reducedX[0,0,3] = x[i,14]
        individualLabel = numpy.zeros((1,1,1))
        individualLabel[0,0,0] = y[i]
        if monthTensorX is None:
            monthTensorX = reducedX
            monthTensorY = individualLabel
        elif monthCounter < 13:
            monthTensorX = numpy.concatenate((monthTensorX,reducedX),axis=1)
            monthTensorY = numpy.concatenate((monthTensorY, individualLabel), axis=1)

        else:
            if dengueTensorX is None:
                dengueTensorX = monthTensorX
                dengueTensorY = monthTensorY
            else:
                dengueTensorX = numpy.concatenate((dengueTensorX, monthTensorX), axis=0)
                dengueTensorY = numpy.concatenate((dengueTensorY, monthTensorY), axis=0)
            monthCounter = 0
            monthTensorX = None
            monthTensorY = None
    print("Dengue X tensor shape: " + str(dengueTensorX.shape) + ", y: " + str(dengueTensorY.shape))
    return dengueTensorX, dengueTensorY

monthlyDengueEstimate = 1

def postProcessMonthlyCases(predictedY, actualY):
    normalizedCases = numpy.zeros(actualY.shape)
    for j in range(actualY.shape[0]):
        actualAnnualTotal = 0
        predictedAnnualTotal = 0
        for i in range(actualY.shape[1]):
            actualAnnualTotal += actualY[j, i]
            predictedAnnualTotal += predictedY[j, i]
        for i in range(actualY.shape[1]):
            normalizedCases[j, i, 0] = \
                int(round(((predictedY[j, i,0 ] * 1.0) / predictedAnnualTotal) * actualAnnualTotal))
    return normalizedCases


def trainNetwork(trainX, trainY, testX, testY):
    models = []
    input_dim = trainX.shape[2]
    input_length = trainX.shape[1]
    output_length = trainY.shape[1]
    output_dim = trainY.shape[2]
    hidden_dim = 15

    #Create our neural network
    #   Here we need to use
    model = Sequential()
    #Input layer
    model.add(LSTM(100,
                   input_shape=(trainX.shape[1], trainX.shape[2]),
                   return_sequences=True,
                   batch_size=trainX.shape[0],
                   activation="relu"))
    #Hidden layers
    model.add(Dense(75, activation="relu"))
    model.add(Dense(50, activation="relu"))
    model.add(Dense(25, activation="relu"))
    #Output layer
    model.add(Dense(trainY.shape[2], activation="relu"))
    model.compile(loss='mse', optimizer='rmsprop')
    model.summary()
    model.fit(trainX, trainY, epochs=750)

    # model.fit(trainX, trainY, epochs=50)
    yHat = model.predict(testX)
    yHat = postProcessMonthlyCases(yHat, testY)

    pyplot.plot(yHat[1])
    pyplot.plot(testY[1])
    pyplot.show()
    print("Train x: " + str(testX[1]))
    print("Train y: " + str(trainY[1]))
    print("Actual y: " + str(testY[1]))
    print("Predicted y: " + str(yHat[1]))

    pyplot.clf()
    pyplot.plot(yHat[2])
    pyplot.plot(testY[2])
    print("Train x: " + str(testX[2]))
    pyplot.show()


    pyplot.clf()
    pyplot.plot(yHat[3])
    pyplot.plot(testY[3])
    print("Train x: " + str(testX[3]))
    pyplot.show()


    pyplot.clf()
    pyplot.plot(yHat[4])
    pyplot.plot(testY[4])
    print("Train x: " + str(testX[4]))
    pyplot.show()

    pyplot.clf()
    pyplot.plot(yHat[5])
    pyplot.plot(testY[5])
    print("Train x: " + str(testX[5]))
    pyplot.show()

def readDataFilesIntoMemory(disease_data_directory, environmental_data_directory):
    # Array used for printing information
    #In order: LST_Day_1km_min[13], precipitation_sum, precipitation[7],
    # and LST_Day_1km_max[14] were most correlated with incidence. Interestingly, population[5] was fifth.

    indyVarNames = ["slope", "elevation", "aspect", "tree_canopy_cover", "landcover",
                    "population", "avg_rad", "precipitation", "precipitation_sum",
                    "precipitation_mean", "precipitation_sum_lag", "precipitation_mean_lag",
                    "LST_DAY_1km_mean", "LST_Day_1km_min", "LST_Day_1km_max"]
    indyVarCorrelations = {"slope" : 0.0, "elevation": 0.0, "aspect": 0.0, "tree_canopy_cover": 0.0,
                           "landcover" : 0.0,
                    "population" : 0.0, "avg_rad" : 0.0, "precipitation" : 0.0, "precipitation_sum" : 0.0,
                    "precipitation_mean" : 0.0, "precipitation_sum_lag" : 0.0, "precipitation_mean_lag" : 0.0,
                    "LST_DAY_1km_mean" : 0.0, "LST_Day_1km_min" : 0.0, "LST_Day_1km_max" : 0.0}
    indyVarCorrelationsMedian = \
        {"slope": [], "elevation": [], "aspect": [], "tree_canopy_cover": [],
                        "landcover": [],
                        "population": [], "avg_rad": [], "precipitation": [], "precipitation_sum": [],
                        "precipitation_mean": [], "precipitation_sum_lag": [], "precipitation_mean_lag": [],
                        "LST_DAY_1km_mean": [], "LST_Day_1km_min": [], "LST_Day_1km_max": []}
    months = []
    year = "2008"
    month = "01"
    day = "01"
    disease_data_beginning = "incidence_province_"
    environmental_data_beginning = "drivers_province_"
    file_ending = ".csv"

    allX = None
    allY = None
    for i in range(25):
        environmental_data_filename = ''.join(
            [environmental_data_beginning, year,"-",month,"-",day,file_ending])
        disease_data_filename = ''.join(
            [disease_data_beginning,year , "-" , month, "-", day, file_ending])
        #Indy data
        independentData = importDengueIndyVariables(os.path.join(environmental_data_directory,
                                                                 environmental_data_filename))
        #Dependent data
        dependentData = importDengueIncidenceRates(
            os.path.join(disease_data_directory, disease_data_filename))
        if numpy.any(numpy.equal(allX, None)):
            allX = independentData
            allY = dependentData
        else:
            allX = numpy.concatenate((allX,independentData),axis=0)
            allY = numpy.concatenate((allY,dependentData)  ,axis=0)

        #Our month object, used for convenience
        monthData = MonthData(independentData,dependentData)
        #Flattened dependent variables, used for correlation analysis

        yFlattened = monthData.y.flatten()
        # Calculate correlation of all features (monthly scale)
        for j in range(15):
            indyCorrelation = calculateCorrelation(monthData.x[:, j], yFlattened, indyVarNames[j])
            indyVarCorrelation = indyVarCorrelations[indyVarNames[j]] + indyCorrelation
            indyVarCorrelations[indyVarNames[j]] = indyVarCorrelation
            indyVarCorrelationsMedian[indyVarNames[j]].append(indyCorrelation)

        integer_month = int(month)
        if integer_month == 12:
            month = "01"
            year = "2009"
        elif integer_month <= 8:
            month = "0" + str(integer_month + 1)
        else:
            month = str(integer_month + 1)

    print("Data shape: " + str(allX.shape))
    monthlyDengueX, monthlyDengueY = convertToSeries(allX, allY)

    train_size = int(monthlyDengueX.shape[0] * 0.67)
    X_train, X_test = monthlyDengueX[0:train_size, :], monthlyDengueX[train_size:monthlyDengueX.shape[0], :]
    y_train, y_test = monthlyDengueY[0:train_size, :], monthlyDengueY[train_size:monthlyDengueX.shape[0], :]
    trainNetwork(X_train, y_train, X_test, y_test)

    # averageAccuracy = 0.0
    # rs = ShuffleSplit(n_splits=2, test_size=.25, random_state=0)
    # for train_index, test_index in rs.split(monthlyDengueX):
    #     X_train, X_test = monthlyDengueX[train_index], monthlyDengueX[test_index]
    #     y_train, y_test = monthlyDengueY[train_index], monthlyDengueY[test_index]
    #     trainNetwork(X_train, y_train, X_test, y_test)
        #print("Individual test accuracy: " + str(accuracy))
        #averageAccuracy += accuracy

    sys.exit(0)
    #Find average correlations of each feature (all data, no longer using the monthly scale)
    yFlattened = allY.flatten()
    for k in range(15):
        indyCorrelation = calculateCorrelation(allX[:, k], yFlattened, indyVarNames[k])
        print(indyVarNames[k] +", full correlation: " + str(indyCorrelation))
        # #Calculate the average correlation
        # indyVarCorrelation = indyVarCorrelations[indyVarNames[k]]
        # indyVarCorrelations[indyVarNames[k]] = indyVarCorrelation / 25.0
        # #Calculate the median correlation
        # print(indyVarNames[k] + ": " +
        #       str(numpy.median(numpy.array(indyVarCorrelations[indyVarNames[k]]))))
    #print("Correlations: " + str(indyVarCorrelations))

    #Perform normal regression analysis
    dataset = pandas.DataFrame({"slope" : allX[:,0], "elevation" : allX[:,1], "aspect" : allX[:,2],
                                "tree_canopy_cover": allX[:,3], "landcover": allX[:,4],"population": allX[:,5],
                                "avg_rad": allX[:,6], "precipitation": allX[:,7], "precipitation_sum": allX[:,8],
                    "precipitation_mean": allX[:,9], "precipitation_sum_lag": allX[:,10],
                    "precipitation_mean_lag": allX[:,11],"LST_Day_1km_mean": allX[:,12],
                                "LST_Day_1km_min": allX[:,13], "LST_Day_1km_max": allX[:,14],
                                'dengue': allY.flatten()})
    regressionModel = smf.ols(formula='dengue ~ slope + '
                                      'elevation + aspect + tree_canopy_cover'
                                      '+ landcover + population + avg_rad + precipitation + precipitation_sum'
                                      '+ precipitation_mean + precipitation_sum_lag + precipitation_mean_lag'
                                      '+ LST_Day_1km_mean + LST_Day_1km_min + LST_Day_1km_max',
                              data=dataset)
    #print(regressionModel.fit().summary())
    clf = linear_model.Lasso(alpha=0.3, max_iter=100000)
    clf.fit(allX, allY)
    print("LASSO Regression Coefficients: ")
    print(clf.coef_)


    poisson_model = sm.GLM(allY, allX, family=sm.families.Poisson())

    #Let's calculate the expectation of y given x
    #   That is- E(Y | x) = e ^ Theta.transposex

    poisson_regression = poisson_model.fit()

    print("\n\nPoisson Regression Results: ")
    for i in range(30):
        test_point_actual_label = allY[i]
        test_point = allX[i,:]
        print("Actual label: " + str(test_point_actual_label) + ", predicted label: "
              + str(poisson_regression.predict(test_point)))


def runRegression(disease_data_directory, environmental_data_directory):
    readDataFilesIntoMemory(disease_data_directory, environmental_data_directory)

def main():
    #Disease data directory
    disease_data_directory = \
        "/home/ktyler/Documents/vast/cambodia/edited_data/vast/incidence_province"
    #Environmental data directory
    environmental_data_directory = \
        "/home/ktyler/Documents/vast/cambodia/edited_data/vast/drivers_province"
    #Perform regression
    runRegression(disease_data_directory, environmental_data_directory)

main()