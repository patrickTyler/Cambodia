import numpy
import pandas
import statsmodels.formula.api as smf
from scipy.stats import linregress
import os
import sys

def importDengueIndyVariables(csvFile):
    dataframe=pandas.read_csv(csvFile, usecols=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16],
                                index_col=False,engine='python')
    dataframe = numpy.array(dataframe)
    return dataframe

def importDengueIncidenceRates(csvFile):
    dataframe = pandas.read_csv(csvFile, usecols=[2],
                                index_col=False, engine='python')
    dataframe = numpy.array(dataframe)
    return dataframe

def readDataIntoMemory(directory, dataType):
    data = None
    for filename in os.listdir(directory):
            if filename.endswith(".csv"):
                if dataType == "independent":
                    if numpy.any(numpy.equal(data,None)):
                        data = importDengueIndyVariables(os.path.join(directory, filename))
                    else:
                        data = numpy.concatenate((
                            data,importDengueIndyVariables(os.path.join(directory, filename))),axis=0)
                else:
                    if numpy.any(numpy.equal(data,None)):
                        data = importDengueIncidenceRates(os.path.join(directory, filename))
                    else:
                        data = numpy.concatenate((
                            data, importDengueIncidenceRates(os.path.join(directory, filename))), axis=0)
                continue
    return data

def calculateCorrelation(indyVariable, dependentVariable, indyVarName):
    slope, intercept, r_value, p_value, std_err = linregress(indyVariable, dependentVariable)
    print(str(indyVarName) + ": " + str(p_value))

def runRegressionAnalysis(drivers_directory, models_directory):
    #Read indepdendent variables into memory
    x = readDataIntoMemory(drivers_directory, "independent")
    # Read depdendent variables into memory
    y = readDataIntoMemory(models_directory, "dependent")
    yFlattened = y.flatten()

    #Array used for printing information
    indyVarNames = ['longitude','latitude','slope','elevation',
                    'tree_canopy_cover', 'landcover', 'population','avg_rad',
                    'precipitation', 'precipitation_sum','precipitation_mean,',
                                 'precipitation_sum_lag', 'precipitation_mean_lag',
                    'LST_Day_1km_mean','LST_Day_1km_min', 'LST_Day_1km_max',
                                 'distanceToWater']
    #Calculate correlation of all features
    for i in range(17):
        calculateCorrelation(x[:,i], yFlattened, indyVarNames[i])

    #Assemble values into pandas dataframe (done
    #  to make regression report more readable
    dataset = pandas.DataFrame({'longitude': x[:,0], 'latitude': x[:,1],
                                'slope' : x[:,2], 'elevation' : x[:,3], 'tree_canopy_cover' :
                                    x[:, 4], 'landcover' : x[:,5], 'population' : x[:,6],
                                'avg_rad' : x[:,7], 'precipitation' : x[:,8], 'precipitation_sum'
                                : x[:,9], 'precipitation_mean' : x[:,10],
                                'precipitation_sum_lag' : x[:,11], 'precipitation_mean_lag' :
                                    x[:, 12], 'LST_Day_1km_mean' : x[:,13],
                                'LST_Day_1km_min' : x[:,14], 'LST_Day_1km_max' : x[:,15],
                                'distanceToWater' : x[:,16], 'dengue' : y.flatten()})
    regressionModel = smf.ols(formula='dengue ~ longitude + '
                  'latitude + slope + elevation + tree_canopy_cover'
                 '+ landcover + population + avg_rad + precipitation + precipitation_sum'
                 '+ precipitation_mean + precipitation_sum_lag + precipitation_mean_lag'
                 '+ LST_Day_1km_mean + LST_Day_1km_min + LST_Day_1km_max + distanceToWater', data=dataset)

    print(regressionModel.fit().summary())



drivers_directory = "/home/ktyler/Documents/vast/cambodia/data/drivers"
models_directory = "/home/ktyler/Documents/vast/cambodia/data/model"

runRegressionAnalysis(drivers_directory, models_directory)

#Whether an individual can make a quantitative argument or not, we don'

