import numpy
import pandas
import numpy
actualY = numpy.random.random((30,12,1))
predictedY = numpy.random.random((30,12,1))
normalizedCases = numpy.zeros(actualY.shape)
for j in range(actualY.shape[0]):
    actualAnnualTotal = 0
    predictedAnnualTotal = 0
    for i in range(actualY.shape[1]):
        actualAnnualTotal += actualY[j,i]
        predictedAnnualTotal += predictedY[j,i]
    for i in range(actualY.shape[1]):
        normalizedCases[j,i,0] = \
            int(round(((predictedY[j,i] * 1.0) / predictedAnnualTotal) * actualAnnualTotal))
#print("shape: " + str(predictedY.shape))
# for i in range(actualY.shape[0]):
#     normalizedCases[i,0] = \
#         int(round(((predictedY[i] * 1.0) /
#          predictedAnnualTotal) * actualAnnualTotal))
#
#
